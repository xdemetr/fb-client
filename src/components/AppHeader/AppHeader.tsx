import { List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import GroupIcon from '@material-ui/icons/Group';
import SportsSoccerIcon from '@material-ui/icons/SportsSoccer';
import LockIcon from '@material-ui/icons/Lock';
import PersonAddIcon from '@material-ui/icons/PersonAdd';

import { ROUTE_GENERATOR, ROUTE_LOGIN, ROUTE_NEW_PLAYER, ROUTE_PLAYERS } from 'const/Routes';
import { TXT_ADD_PLAYER, TXT_GENERATOR, TXT_LOGIN, TXT_LOGOUT, TXT_PLAYERS } from 'const/Vars';
import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import * as actions from 'store/actions/auth';

import { getAuthReselect } from 'store/selectors/auth';
import { AppState } from 'store/store';

interface IProps {
  auth: {
    isAuth?: boolean;
  };
  logoutUser: () => void;
}

const useStyles = makeStyles((theme: Theme) => createStyles(
  {
    active: {
      backgroundColor: theme.palette.grey['100'],
    },
  }),
);

const AppHeader: React.FC<IProps> = ({ auth: { isAuth }, logoutUser }) => {
  const links = [
    {
      access: 'all', icon: 'generator', id: 'link01', label: TXT_GENERATOR, link: ROUTE_GENERATOR,
    },
    // {
    //   access: 'all', id: 'link02', label: TXT_RESULTS, link: ROUTE_PLAYDAYS,
    // },
    {
      access: 'all', icon: 'players', id: 'link03', label: TXT_PLAYERS, link: ROUTE_PLAYERS,
    },
    {
      access: 'auth',
      icon: 'player-add',
      id: 'link04',
      label: TXT_ADD_PLAYER,
      link: ROUTE_NEW_PLAYER,
    },
    {
      access: 'guest', icon: 'login', id: 'link05', label: TXT_LOGIN, link: ROUTE_LOGIN,
    },
    {
      access: 'auth',
      action: () => logoutUser(),
      icon: 'login',
      id: 'link06',
      label: TXT_LOGOUT,
      link: '',
    },
  ];

  const classes = useStyles();

  interface ILink {
    access: string;
    action?: () => void;
    id: string;
    link: string;
    label: string;
    icon: string;
  }

  function generateLinks(list: ILink[], type: 'auth' | 'guest') {
    if (!list.length) return null;

    return list.filter(el => (el.access === type || el.access === 'all'))
      .map(({ icon, id, link, label, action }) => {
        const Icon = () => {
          switch (icon) {
            case 'generator': {
              return <SportsSoccerIcon/>;
            }

            case 'players': {
              return <GroupIcon/>;
            }

            case 'player-add': {
              return <PersonAddIcon/>;
            }

            case 'login': {
              return <LockIcon/>;
            }

            default: {
              return null;
            }
          }
        };

        return (
          <ListItem
            button={true}
            component={NavLink}
            key={id}
            to={link}
            onClick={action}
            activeClassName={link && classes.active}
          >
            <ListItemIcon><Icon/></ListItemIcon>
            <ListItemText primary={label}/>
          </ListItem>
        );
      });
  }

  const navLinks = isAuth ? generateLinks(links, 'auth') : generateLinks(links, 'guest');

  return (
    <List>
      {navLinks}
    </List>
  );
};

const mapStateToProps = (state: AppState) => ({
  auth: getAuthReselect(state),
});

export default connect(
  mapStateToProps,
  { logoutUser: actions.logoutUser },
)(AppHeader);
