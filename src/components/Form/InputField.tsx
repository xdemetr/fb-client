import {
  Checkbox,
  FormControl,
  FormControlLabel,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from '@material-ui/core';
import React from 'react';

interface IProps {
  name: string;
  type?: 'password' | 'email' | 'checkbox' | 'select' | 'text';
  handleBlur?: (e: any) => void;
  handleChange?: (e: any) => void;
  values?: any;
  touched?: any;
  errors?: any;
  placeholder?: string;
  label?: string;
  autoFocus?: any;
  autoComplete?: string;
  margin?: string;
  children?: any;
}

const Input: React.FC<IProps> = (
  {
    name,
    handleChange,
    values,
    handleBlur,
    type,
    touched, errors,
    placeholder,
    label,
    autoFocus,
    autoComplete = 'false',
  }) => {
  return (
    <TextField
      name={name}
      type={type}
      value={values[name]}
      placeholder={placeholder}
      onChange={handleChange}
      onBlur={handleBlur}
      error={touched[name] && errors[name]}
      label={label}
      fullWidth={true}
      helperText={(touched[name] && errors[name]) ? errors[name] : null}
      autoFocus={autoFocus}
      autoComplete={autoComplete}
      variant="outlined"
      margin="normal"
    />
  );
};

const CheckboxField: React.FC<IProps> = (
  {
    name,
    handleChange,
    values,
    handleBlur,
    label,
  }) => {

  const checkBoxControl = (
    <Checkbox
      id={name}
      checked={values[name]}
      onChange={handleChange}
      onBlur={handleBlur}
      name={name}
      color="primary"
      inputProps={{ 'aria-label': label }}
    />
  );

  return (
    <FormControlLabel
      control={checkBoxControl}
      label={label}
    />
  );
};

const SelectField: React.FC<IProps> = (
  {
    name,
    handleChange,
    values,
    handleBlur,
    touched,
    errors,
    children,
    label,
  }) => {

  const items = children.map((item: any) => {
    return (
      <MenuItem
        value={item.props.value}
        key={item.props.value}
      >
        {item.props.children}
      </MenuItem>
    );
  });

  return (

    <FormControl
      error={touched[name] && errors[name]}
      variant="outlined"
      fullWidth={true}
      margin="normal"
    >
      <InputLabel htmlFor={name}>{label}</InputLabel>
      <Select
        id={name}
        name={name}
        value={values[name]}
        onChange={handleChange}
        onBlur={handleBlur}
        variant="outlined"
        label={label}
      >
        {items}
      </Select>
    </FormControl>
  );
};

const InputField: React.FC<IProps> = (props) => {
  const { name, type = 'text', ...rest } = props;

  let renderInput;

  switch (type) {
    case 'text':
    case 'email':
    case 'password': {
      renderInput = <Input name={name} type={type} {...rest} />;
      break;
    }

    case 'checkbox': {
      renderInput = <CheckboxField name={name} type="checkbox" {...rest} />;
      break;
    }

    case 'select': {
      renderInput = <SelectField name={name} {...rest} />;
      break;
    }
    default: {
      renderInput = null;
    }
  }

  return renderInput;
};

export default InputField;
