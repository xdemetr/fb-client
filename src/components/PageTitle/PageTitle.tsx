import { Typography } from '@material-ui/core';
import React from 'react';

const PageTitle: React.FC<{ children: any }> = ({ children }) => {
  if (!children) return null;

  return (
    <Typography component="h1" variant="h4" gutterBottom={true}>
      {children}
    </Typography>
  );
};

export default PageTitle;
