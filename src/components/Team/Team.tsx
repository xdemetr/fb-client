import {
  Avatar,
  Badge,
  IconButton,
  List,
  ListItem,
  ListItemAvatar,
  ListItemSecondaryAction,
  ListItemText,
  Paper,
  Typography,
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/DeleteOutlined';
import React from 'react';
import { Link } from 'react-router-dom';

import IPlayer from 'types/interface/IPlayer';
import { ROUTE_PLAYERS } from '../../const/Routes';
import './Team.css';

interface IProps {
  players: IPlayer[];
  goals?: number;
  color: number;
  deletePlayerFromTeam?: any;
  auth?: { isAuth: boolean };
  view?: 'compact' | 'default';
  withoutLink?: boolean;
}

const useStyles = makeStyles((theme: Theme) => createStyles(
  {
    avatarBlue: {
      border: '4px solid #3f51b5',
    },
    avatarRed: {
      border: '4px solid #f44336',
    },
    blue: {
      color: theme.palette.primary.main,
    },
    disabled: {
      pointerEvents: 'none',
    },
    link: {
      color: theme.palette.grey.A700,
    },
    red: {
      color: theme.palette.error.main,
    },
    white: {
      color: theme.palette.primary.main,
    },
  }),
  )
;

const Team: React.FC<IProps> = (
  {
    players,
    goals,
    color,
    deletePlayerFromTeam,
    auth = false,
    view = 'default',
    withoutLink = false,
  }) => {
  const classes = useStyles();

  const teamData = [
    { color: 'red', title: 'Красные незабудки' },
    { color: 'blue', title: 'Синие снегири' },
    { color: 'white', title: 'Белые викинги' },
  ];

  if (!players.length) {
    return null;
  }

  const Total = () => {
    if (!goals) {
      return null;
    }

    return (
      <ListItemSecondaryAction>
        <Badge badgeContent={goals} color="primary">&nbsp;</Badge>
      </ListItemSecondaryAction>
    );
  };

  let teamColor = '';
  let teamAvatarColor = '';
  switch (color) {
    case 0: {
      teamColor = classes.red;
      teamAvatarColor = classes.avatarRed;
      break;
    }
    case 1: {
      teamColor = classes.blue;
      teamAvatarColor = classes.avatarBlue;
      break;
    }
  }

  const list = players.map((player) => {
    const { _id, image, name, handle } = player;

    const DeleteButton = () => {
      if (!auth) {
        return null;
      }

      return (
        <IconButton
          edge="end"
          aria-label="delete"
          onClick={() => deletePlayerFromTeam(player, color)}
        >
          <DeleteIcon fontSize="small"/>
        </IconButton>
      );
    };

    return (
      <ListItem
        dense={true}
        button={true}
        key={_id}
        component={Link}
        to={`${ROUTE_PLAYERS}${handle}`}
      >
        <ListItemAvatar>
          <Avatar alt={name} src={image} className={teamAvatarColor}/>
        </ListItemAvatar>
        <ListItemText primary={name}/>

        <ListItemSecondaryAction>
          <DeleteButton/>
        </ListItemSecondaryAction>
      </ListItem>
    );
  });

  return (
    <List component={Paper}>
      <ListItem className={teamColor} component={Typography} variant="h6">
        {teamData[color].title}
        <Total/>
      </ListItem>
      {list}
    </List>
  );
};

export default React.memo(Team);
