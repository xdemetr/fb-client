import { Button, Grid, Typography } from '@material-ui/core';

import Error from 'components/Error';
import Team from 'components/Team';

import { TXT_RESULTS, TXT_SAVE } from 'const/Vars';
import React from 'react';
import ITeam from 'types/interface/ITeam';

interface IProps {
  result: ITeam[];
  errors?: string;
  auth: {
    isAuth: boolean;
  };
  onSaveResult: () => void;
  saveBtnClass: string;
}

const GeneratorResult: React.FC<IProps> = (
  {
    result,
    onSaveResult,
    errors,
    auth: { isAuth },
    saveBtnClass,
  }) => {
  if (!result.length) {
    return null;
  }

  const results = result.map((team: any, idx: number) => {
    if (!Object.keys(team).length) {
      return null;
    }

    return (
      <Grid item={true} xs={12} sm={6} md={3} key={idx}>
        <Team players={team} color={idx}/>
      </Grid>
    );
  });

  const SaveButton = () => {
    if (!isAuth) {
      return null;
    }
    return (
      <Button
        className={saveBtnClass}
        onClick={onSaveResult}
        size="large"
        color="primary"
        variant="contained"
      >
        {TXT_SAVE}
      </Button>
    );
  };

  return (
    <div className="generator-result">
      <Typography component="h2" variant="h5" gutterBottom={true}>
        {TXT_RESULTS}
      </Typography>

      <Error message={errors}/>

      <Grid container={true} spacing={3}>
        {results}
      </Grid>
      <SaveButton/>
    </div>
  );
};

export default React.memo(GeneratorResult);
