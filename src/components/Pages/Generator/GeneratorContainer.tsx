import { Badge, Button, ButtonGroup, Grid } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import Error from 'components/Error';
import Spinner from 'components/Spinner';

import {
  GENERATOR_MIN_SIZE,
  TXT_GENERATOR_GET_TEAMS,
  TXT_PAGE_GENERATOR,
  TXT_RESET,
} from 'const/Vars';
import React, { useEffect } from 'react';

import { connect } from 'react-redux';

import * as generatorActions from 'store/actions/generator';
import * as playerActions from 'store/actions/player';
import { getAuthReselect } from 'store/selectors/auth';
import {
  getGeneratorErrors,
  getGeneratorResultReselect,
  getSelectedPlayersReselect,
} from 'store/selectors/generator';

import { getPlayerErrorReselect, getPlayerListReselect } from 'store/selectors/player';
import { AppState } from 'store/store';

import IPlayer from 'types/interface/IPlayer';
import ITeam from 'types/interface/ITeam';
import PageTitle from '../../PageTitle';
import PlayerCard from '../../PlayerCard/PlayerCard';
import GeneratorResult from './GeneratorResult';

interface IProps {
  auth: {
    isAuth: boolean;
  };
  // list: IPlayer[];
  // list: {
  //   data: IPlayer[],
  //   pageCount: number,
  // };
  list: any;
  selected: IPlayer[];
  result: ITeam[];
  error?: string;
  errors?: string;
  generatorPlayerSelected: (player: IPlayer) => void;
  generatorPlayersReset: () => void;
  generatorRun: (teams: any[]) => void;
  generatorSaveResult: (result: ITeam[]) => void;
  getPlaydays: () => void;
  getPlayers: () => void;
}

const useStyles = makeStyles((theme: Theme) => createStyles(
  {
    buttonGroup: {
      marginBottom: theme.spacing(2),
    },
    content: {
      marginBottom: theme.spacing(3),
    },
    saveButton: {
      marginTop: theme.spacing(1),
    },
    selected: {
      background: theme.palette.info.main,
      color: theme.palette.info.contrastText,
    },
  }),
);

const GeneratorContainer: React.FC<IProps> = (
  {
    auth,
    list, selected, result, error, errors,
    generatorPlayerSelected, generatorPlayersReset, generatorRun, generatorSaveResult,
    getPlayers,
  }) => {

  const classes = useStyles();

  useEffect(
    () => {
      getPlayers();
    },
    [getPlayers]);

  if (!list.data) {
    return <Spinner/>;
  }

  if (error) {
    return <Error message={error}/>;
  }

  const playerList = list.data.map((player: IPlayer) => {
    const { _id: id } = player;
    const selectedClass = selected.find(pl => pl._id === id) ? classes.selected : '';

    return (
      <Grid item={true} xs={12} sm={6} md={3} key={id}>
        <PlayerCard
          auth={false}
          player={player}
          onClick={() => generatorPlayerSelected(player)}
          selected={selectedClass}
        />
      </Grid>
    );
  });

  const selectedCount = () => {
    if (!selected.length) {
      return null;
    }
    return selected.length;
  };

  const BtnGroup = () => {
    return (
      <ButtonGroup color="primary" className={classes.buttonGroup}>
        <Button
          size="large"
          disabled={selected.length >= GENERATOR_MIN_SIZE ? false : true}
          color="primary"
          variant="contained"
          onClick={onGenerateClick}
        >
          {TXT_GENERATOR_GET_TEAMS}
        </Button>

        <Button
          size="large"
          disabled={selected.length >= GENERATOR_MIN_SIZE ? false : true}
          onClick={generatorPlayersReset}
        >
          {TXT_RESET}
        </Button>
      </ButtonGroup>
    );
  };

  const onGenerateClick = () => {
    const players = selected;

    const getRandomInt = (min: number, max: number) => {
      return Math.floor(Math.random() * (max - min)) + min;
    };

    const playerToTeam = (team: IPlayer[], box1: IPlayer[], box2: IPlayer[], box3: IPlayer[]) => {
      let random = 0;
      let startList = box1;

      if (startList.length < 1) {
        startList = box2;
      }
      if (startList.length < 1) {
        startList = box3;
      }
      if (startList.length > 0) {
        random = getRandomInt(0, (startList.length));
        team.push(startList[random]);
        startList.splice(random, 1);
      }
      return team;
    };

    const resBox1 = players.filter(e => e.box === 1);
    const resBox2 = players.filter(e => e.box === 2);
    const resBox3 = players.filter(e => e.box === 3);

    let team1: IPlayer[] = [];
    let team2: IPlayer[] = [];
    let team3: IPlayer[] = [];

    players.map(() => {
      team1 = playerToTeam(team1, resBox1, resBox2, resBox3);
      team2 = playerToTeam(team2, resBox1, resBox2, resBox3);

      if (players.length >= 15) {
        team3 = playerToTeam(team3, resBox1, resBox2, resBox3);
      }

      return [team1, team2, team3];
    });

    generatorRun([team1, team2, team3]);
  };

  const onSaveResult = () => {
    generatorSaveResult(result);
  };

  return (
    <div className="generator-page">
      <PageTitle>
        <Badge badgeContent={selectedCount()} color="primary">
          {TXT_PAGE_GENERATOR}
        </Badge>
      </PageTitle>

      <Grid container={true} spacing={3} className={classes.content}>
        {playerList}
      </Grid>

      <BtnGroup/>

      <GeneratorResult
        auth={auth}
        errors={errors}
        result={result}
        onSaveResult={onSaveResult}
        saveBtnClass={classes.saveButton}
      />
    </div>
  );
};

const mapStateToProps = (state: AppState) => ({
  auth: getAuthReselect(state),
  error: getPlayerErrorReselect(state),
  errors: getGeneratorErrors(state),
  list: getPlayerListReselect(state),
  result: getGeneratorResultReselect(state),
  selected: getSelectedPlayersReselect(state),
});

const mapDispatchToProps = {
  generatorPlayerSelected: generatorActions.generatorPlayerSelected,
  generatorPlayersReset: generatorActions.generatorPlayersReset,
  generatorRun: generatorActions.generatorRun,
  generatorSaveResult: generatorActions.generatorSaveResult,
  getPlayers: playerActions.getPlayers,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(React.memo(GeneratorContainer));
