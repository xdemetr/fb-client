import { Grid, IconButton, Typography } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';

import Team from 'components/Team';

import { ROUTE_EDIT_PLAYDAY } from 'const/Routes';
import { TXT_EDIT } from 'const/Vars';
import React from 'react';

import { Link } from 'react-router-dom';
import IPlayday from 'types/interface/IPlayday';

interface IProps {
  playday: IPlayday | null;
  auth: boolean;
}

const PlaydaysDetails: React.FC<IProps> = ({ playday, auth }) => {
  if (!playday) {
    return null;
  }

  const teamList = playday.teams.map((team, idx) => (
     <Grid item={true} xs={12} sm={6} md={3} key={idx}>
       <Team
         players={team}
         goals={playday.goals[idx]}
         color={idx}
         view="compact"
       />
     </Grid>
   ),
  );

  const EditLink = () => {
    return (
      <IconButton
        component={Link}
        to={`${ROUTE_EDIT_PLAYDAY}${playday._id}`}
        color="primary"
        aria-label={TXT_EDIT}
      >
        <EditIcon fontSize="small"/>
      </IconButton>
    );
  };

  const editLink = auth ? <EditLink/> : null;

  return (
    <div className="playdays-details">

      <Typography component="h2" variant="h5" gutterBottom={true}>
        {playday.name}
        {editLink}
      </Typography>

      <Grid container={true} spacing={3}>
        {teamList}
      </Grid>
    </div>
  );
};

export default PlaydaysDetails;
