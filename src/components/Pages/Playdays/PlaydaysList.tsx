import { Divider, List, ListItem, ListSubheader, Typography } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';

import { ROUTE_PLAYDAYS } from 'const/Routes';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { compose } from 'redux';
import { getPlaydayReselect, getPlaydaysReselect } from 'store/selectors/playday';
import { TXT_RESULTS } from '../../../const/Vars';
import * as actions from '../../../store/actions/playday';
import { AppState } from '../../../store/store';
import IPlaydayList from '../../../types/interface/IPlaydayList';
import Spinner from '../../Spinner';

interface IProps {
  getPlaydays: (limit?: number, pageNumber?: number) => void;
  list: { data: IPlaydayList[] | never, pageCount: number };
}

const useStyles = makeStyles((theme: Theme) => createStyles(
  {
    active: {
      backgroundColor: theme.palette.grey['100'],
    },
    pagination: {
      marginBottom: theme.spacing(2),
      marginTop: theme.spacing(2),
    },
  }),
);

const PlaydaysList: React.FC<IProps> = ({ getPlaydays, list }) => {
  const [currentPage, setPage] = React.useState(1);
  const handlePageChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  useEffect(
    () => {
      getPlaydays(10, currentPage);
    },
    [getPlaydays, currentPage]);

  const classes = useStyles();

  if (!list.data) {
    return <Spinner/>;
  }

  const items = list.data.map(({ _id, name }) => {
    return (
      <ListItem
        button={true}
        component={NavLink}
        key={_id}
        to={`${ROUTE_PLAYDAYS}${_id}`}
        activeClassName={classes.active}
      >
        {name}
      </ListItem>
    );
  });

  const PlayDayPagination = () => {
    if (list.pageCount <= 1) return null;

    return (
      <Typography component="section">
        <Divider/>
        <Pagination
          count={list.pageCount}
          page={currentPage}
          onChange={handlePageChange}
          size="small"
          siblingCount={0}
          className={classes.pagination}
        />
      </Typography>
    );
  };

  return (
    <List>
      <ListSubheader disableSticky={true}>{TXT_RESULTS}</ListSubheader>
      {items}
      <PlayDayPagination/>
    </List>
  );
};

const mapStateToProps = (state: AppState) => ({
  current: getPlaydayReselect(state),
  list: getPlaydaysReselect(state),
});

const mapDispatchToProps = {
  getPlayday: actions.getPlayday,
  getPlaydays: actions.getPlaydays,
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
)(React.memo(PlaydaysList));
