import Error from 'components/Error';
import Spinner from 'components/Spinner';

import { TXT_RESULTS } from 'const/Vars';
import React, { useEffect } from 'react';

import { connect } from 'react-redux';
import { compose } from 'redux';
import * as actions from 'store/actions/playday';
import { getAuthReselect } from 'store/selectors/auth';
import {
  getPlaydayReselect,
  getPlaydaysErrorReselect,
  getPlaydaysLoadingReselect,
  getPlaydaysReselect,
} from 'store/selectors/playday';
import { AppState } from 'store/store';
import IPlayday from 'types/interface/IPlayday';
import PageTitle from '../../PageTitle';
import PlaydaysDetails from './PlaydaysDetails';

interface IProps {
  getPlaydays: () => void;
  getPlayday: (id: string) => void;
  list: any;
  loading: boolean;
  error?: string;
  current: IPlayday | null;
  match: {
    params: {
      id: string | '';
    },
  };
  auth: {
    isAuth: boolean;
  };
}

const PlaydaysContainer: React.FC<IProps> = (
  {
    getPlayday, list, loading,
    error, current, match: { params: { id } },
    auth: { isAuth },
  },
) => {
  useEffect(
    () => {
      if (id) {
        getPlayday(id);
      }
    },
    [getPlayday, id]);

  if (loading) {
    return <Spinner/>;
  }

  if (error) {
    return <Error message={error}/>;
  }

  return (
    <div className="playdays-page">
      <PageTitle>
        {TXT_RESULTS}
      </PageTitle>

      <PlaydaysDetails playday={current} auth={isAuth}/>
    </div>
  );
};

const mapStateToProps = (state: AppState) => ({
  auth: getAuthReselect(state),
  current: getPlaydayReselect(state),
  error: getPlaydaysErrorReselect(state),
  list: getPlaydaysReselect(state),
  loading: getPlaydaysLoadingReselect(state),
});

const mapDispatchToProps = {
  getPlayday: actions.getPlayday,
  getPlaydays: actions.getPlaydays,
};

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
)(React.memo(PlaydaysContainer));
