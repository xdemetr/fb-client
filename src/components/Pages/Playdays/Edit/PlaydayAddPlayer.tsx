import { Button } from '@material-ui/core';

import InputField from 'components/Form/InputField';

import { TXT_ADD } from 'const/Vars';

import { useFormik } from 'formik';
import React from 'react';
import IPlayer from 'types/interface/IPlayer';

interface IProps {
  id: number;
  players: IPlayer[];
  onSubmit: (data: any, team: any) => void;
}

const PlaydayAddPlayer: React.FC<IProps> = ({ players, id, onSubmit }) => {
  const loginFormOptions = useFormik(
    {
      initialValues: {},
      onSubmit: (values) => {
        onSubmit(values, id);
      },
    },
  );

  if (!players.length) {
    return null;
  }

  const options = players.map(({ _id, name }: IPlayer) => {
    return <option value={_id} key={_id}>{name}</option>;
  });

  const { handleSubmit, ...props } = loginFormOptions;

  return (
    <div className="playday-add-player">
      <form className="row" onSubmit={handleSubmit}>
        <div className="col-md-8">
          <InputField name={`select_team_${id}`} type="select" {...props} label="Добавить игрока">
            {options}
          </InputField>
        </div>
        <div className="col-md-4">
          <Button
            color="primary"
            size="large"
            type={'submit'}
            fullWidth={true}
          >
            {TXT_ADD}
          </Button>
        </div>
      </form>
    </div>
  );
};

export default PlaydayAddPlayer;
