import { Button, Grid } from '@material-ui/core';
import React from 'react';

import { useFormik } from 'formik';
import * as Yup from 'yup';

import InputField from 'components/Form/InputField';

import { TXT_FIELD_REQUIRED, TXT_GOALS, TXT_SAVE } from 'const/Vars';
import IPlayday from 'types/interface/IPlayday';

interface IProps {
  onSubmit: (formData: any) => void;
  current: IPlayday;
}

const PlaydayEditForm: React.FC<IProps> = ({ onSubmit, current }) => {
  const loginFormOptions = useFormik(
    {
      initialValues: {
        res1: current.goals[0],
        res2: current.goals[1],
      },
      onSubmit: (values) => {
        onSubmit(values);
      },
      validationSchema: Yup.object().shape(
        {
          res1: Yup.string().required(TXT_FIELD_REQUIRED),
          res2: Yup.string().required(TXT_FIELD_REQUIRED),
        },
      ),
    },
  );

  const { handleSubmit, ...props } = loginFormOptions;

  if (!current) {
    return null;
  }

  return (
    <form onSubmit={handleSubmit}>
      <Grid container={true} spacing={3}>
        <Grid item={true} xs={12} sm={6} md={3}>
          <InputField placeholder={TXT_GOALS} name="res1" {...props} autoComplete="off"/>
        </Grid>
        <Grid item={true} xs={12} sm={6} md={3}>
          <InputField placeholder={TXT_GOALS} name="res2" {...props} autoComplete="off"/>
        </Grid>
      </Grid>

      <div className="row">
        <Button type="submit" color="primary" variant="contained">{TXT_SAVE}</Button>
      </div>
    </form>
  );
};

export default React.memo(PlaydayEditForm);
