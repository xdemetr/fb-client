import { Typography } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';

import Error from 'components/Error';
import Spinner from 'components/Spinner';

import { TXT_PAGE_PLAYERS } from 'const/Vars';
import React, { useEffect } from 'react';

import { connect } from 'react-redux';
import { compose } from 'redux';
import * as actions from 'store/actions/player';
import { getAuthReselect } from 'store/selectors/auth';
import {
  getPlayerErrorReselect,
  getPlayerListReselect,
  getPlayerLoadingReselect,
} from 'store/selectors/player';
import { AppState } from 'store/store';
import IPlayer from 'types/interface/IPlayer';
import PageTitle from '../../PageTitle';
import Players from './Players';

interface IProps {
  list: { data: IPlayer[], pageCount: number };
  loading: boolean;
  error: string;
  getPlayers: (limit?: number, pageNumber?: number) => void;
  auth: { isAuth?: boolean };
}

const PlayersContainer: React.FC<IProps> = (
  {
    getPlayers,
    list,
    loading,
    error,
    auth: { isAuth },
  }) => {

  const [currentPage, setPage] = React.useState(1);
  const handlePageChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setPage(value);
  };

  useEffect(
    () => {
      getPlayers(20, currentPage);
    },
    [getPlayers, currentPage]);

  if (loading || !list) {
    return <Spinner/>;
  }

  if (error) {
    return <Error message={error}/>;
  }

  const PlayerPagination = () => {

    if (list.pageCount <= 1) return null;

    return (
      <Typography component="section" gutterBottom={true}>
        <Pagination
          count={list.pageCount}
          page={currentPage}
          onChange={handlePageChange}
          boundaryCount={2}
        />
      </Typography>
    );
  };

  return (
    <div className="players-page">
      <PageTitle>
        {TXT_PAGE_PLAYERS}
      </PageTitle>

      <PlayerPagination/>
      <Players list={list} auth={isAuth}/>
    </div>
  );
};

const mapStateToProps = (state: AppState) => ({
  auth: getAuthReselect(state),
  error: getPlayerErrorReselect(state),
  list: getPlayerListReselect(state),
  loading: getPlayerLoadingReselect(state),
});

export default compose(
  connect(mapStateToProps, { getPlayers: actions.getPlayers }),
)(React.memo(PlayersContainer));
