import { Grid } from '@material-ui/core';
import React from 'react';
import IPlayer from 'types/interface/IPlayer';
import PlayerCard from '../../PlayerCard/PlayerCard';

interface IProps {
  list: {
    data: IPlayer[],
  };
  auth?: boolean;
}

const Players: React.FC<IProps> = ({ list: { data }, auth }) => {

  if (!data) return null;

  const playerList = data.map((player: any) => {
    return (
      <Grid item={true} xs={12} sm={6} md={3} key={player._id}>
        <PlayerCard
          auth={auth}
          player={player}
          showBox={true}
          withLink={true}
        />
      </Grid>
    );
  });

  return (
    <Grid container={true} spacing={3}>
      {playerList}
    </Grid>
  );
};

export default React.memo(Players);
