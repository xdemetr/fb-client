import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import InputField from 'components/Form/InputField';

import {
  TXT_FIELD_REQUIRED,
  TXT_LABEL_BOX,
  TXT_LABEL_BOX_1,
  TXT_LABEL_BOX_2,
  TXT_LABEL_BOX_3,
  TXT_LABEL_DAMAGE,
  TXT_LABEL_IMAGE,
  TXT_LABEL_LOGIN,
  TXT_LABEL_NAME,
  TXT_SAVE,
} from 'const/Vars';

import { useFormik } from 'formik';
import React from 'react';

import IPlayer from 'types/interface/IPlayer';
import { IPlayerFormData } from 'types/interface/IPlayerFormData';
import * as Yup from 'yup';

interface IProps {
  onSubmit: (formData: IPlayerFormData) => void;
  current: IPlayer;
}

const useStyles = makeStyles((theme:any) => ({
  form: {
    marginTop: theme.spacing(2),
    width: '100%',
  },
  submit: {
    margin: theme.spacing(3, 0, 0),
  },
}));

const PostPlayerForm: React.FC<IProps> = ({ onSubmit, current }) => {
  const loginFormOptions = useFormik({
    initialValues: {
      box: current ? current.box : 1,
      damage: current ? current.damage : false,
      handle: current ? current.handle : '',
      image: current ? current.image : '',
      name: current ? current.name : '',
    },
    onSubmit: (values) => {
      onSubmit(values);
    },
    validationSchema: Yup.object().shape({
      box: Yup.string().required(TXT_FIELD_REQUIRED),
      handle: Yup.string().required(TXT_FIELD_REQUIRED),
      name: Yup.string().required(TXT_FIELD_REQUIRED),
    }),
  });

  const classes = useStyles();

  const { handleSubmit, ...props } = loginFormOptions;

  return (
    <form onSubmit={handleSubmit} className={classes.form}>
      <InputField label={TXT_LABEL_NAME} name="name" {...props} />

      <InputField label={TXT_LABEL_LOGIN} name="handle" {...props} />

      <InputField label={TXT_LABEL_IMAGE} name="image" {...props} />

      <InputField name="box" type="select" label={TXT_LABEL_BOX} {...props}>
        <option value="1">{TXT_LABEL_BOX_1}</option>
        <option value="2">{TXT_LABEL_BOX_2}</option>
        <option value="3">{TXT_LABEL_BOX_3}</option>
      </InputField>
      <InputField name="damage" type="checkbox" label={TXT_LABEL_DAMAGE} {...props} />

      <Button
        type="submit"
        fullWidth={true}
        variant="contained"
        color="primary"
        className={classes.submit}
        size="large"
      >
        {TXT_SAVE}
      </Button>

    </form>
  );
};

export default React.memo(PostPlayerForm);
