import { Button, Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import Spinner from 'components/Spinner';

import { TXT_DELETE, TXT_PAGE_ADD_PLAYER, TXT_PAGE_EDIT_PLAYER } from 'const/Vars';
import withAuthRedirect from 'hoc/withAuthRedirect';
import React, { useEffect } from 'react';

import { connect } from 'react-redux';
import { compose } from 'redux';
import * as actions from 'store/actions/player';
import { getPlayerCurrentReselect, getPlayerLoadingReselect } from 'store/selectors/player';
import { AppState } from 'store/store';
import { IPlayerFormData } from 'types/interface/IPlayerFormData';
import PageTitle from '../../../PageTitle';
import PostPlayerForm from './PostPlayerForm';

interface IProps {
  history: any;
  postPlayer: (formData: IPlayerFormData, history: any) => void;
  updatePlayer: (id: string, formData: IPlayerFormData, history: any) => void;
  deletePlayer: (id: string, history: any) => void;
  current: any;
  getCurrentPlayer: (id: string) => void;
  resetCurrentPlayer: () => void;
  match: any;
  loading?: boolean;
}

const useStyles = makeStyles((theme: any) => ({
  delete: {
    margin: theme.spacing(2, 0, 0),
  },
}));

const PostPlayer: React.FC<IProps> = (
  {
    match: { params: { id } },
    postPlayer, updatePlayer, deletePlayer, current,
    getCurrentPlayer, resetCurrentPlayer, history, loading,
  },
) => {
  useEffect(
    () => {
      if (id) {
        getCurrentPlayer(id);
      } else {
        resetCurrentPlayer();
      }
    },
    [getCurrentPlayer, resetCurrentPlayer, id]);

  const onSubmit = (formData: IPlayerFormData) => {
    if (!id) {
      postPlayer(formData, history);
    } else {
      updatePlayer(id, formData, history);
    }
  };

  const classes = useStyles();

  const DeleteButton = () => {
    if (!id) {
      return null;
    }
    return (
      <Button
        fullWidth={true}
        color="secondary"
        onClick={() => deletePlayer(id, history)}
        className={classes.delete}
      >
        {TXT_DELETE}
      </Button>
    );
  };

  const title = id ? TXT_PAGE_EDIT_PLAYER : TXT_PAGE_ADD_PLAYER;

  if (loading) {
    return <Spinner/>;
  }

  return (
    <Container component="section" maxWidth="sm" disableGutters={true}>
      <PageTitle>
        {title}
      </PageTitle>

      <PostPlayerForm
        onSubmit={onSubmit}
        current={current}
      />
      <DeleteButton/>
    </Container>
  );
};

const mapStateToProps = (state: AppState) => ({
  current: getPlayerCurrentReselect(state),
  loading: getPlayerLoadingReselect(state),
});

const mapDispatchToProps = ({
  deletePlayer: actions.deletePlayer,
  getCurrentPlayer: actions.getCurrentPlayer,
  postPlayer: actions.postPlayer,
  resetCurrentPlayer: actions.resetCurrentPlayer,
  updatePlayer: actions.updatePlayer,
});

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  withAuthRedirect,
)(React.memo(PostPlayer));
