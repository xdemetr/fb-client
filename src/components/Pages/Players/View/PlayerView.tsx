import { Avatar, Card, CardContent, CardHeader, Container, Typography } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import Spinner from 'components/Spinner';

import { TXT_LABEL_BOX } from 'const/Vars';
import React, { useEffect } from 'react';

import { connect } from 'react-redux';
import { compose } from 'redux';
import * as actions from 'store/actions/player';
import { getPlayerCurrentReselect, getPlayerLoadingReselect } from 'store/selectors/player';
import { AppState } from 'store/store';
import IPlayer from 'types/interface/IPlayer';
import PlayerPlaydayList from './PlayerPlaydayList';

interface IProps {
  match: any;
  getPlayerByHandle: (handle: string) => void;
  current: IPlayer | null;
  loading: boolean;
}

const useStyles = makeStyles((theme: Theme) => createStyles(
  {
    avatar: {
      height: theme.spacing(7),
      width: theme.spacing(7),
    },
  }),
);

const PlayerView: React.FC<IProps> = (
  {
    match: { params: { handle } },
    getPlayerByHandle,
    current,
    loading,
  },
) => {
  const classes = useStyles();

  useEffect(
    () => {
      getPlayerByHandle(handle);
    },
    [getPlayerByHandle, handle]);

  if (!current) {
    return null;
  }
  if (loading) {
    return <Spinner/>;
  }

  const { name, image, box, playdays } = current;

  return (
    <Container maxWidth="md" disableGutters={true}>
      <Card>
        <CardHeader
          title={<Typography variant="h5">{name}</Typography>}
          avatar={<Avatar alt={name} src={image} className={classes.avatar}/>}
          action={<span>{TXT_LABEL_BOX}: {box}</span>}
        />

        <CardContent>
          <PlayerPlaydayList playdays={playdays}/>
        </CardContent>
      </Card>
    </Container>
  );
};

const mapStateToProps = (state: AppState) => ({
  current: getPlayerCurrentReselect(state),
  loading: getPlayerLoadingReselect(state),
});

export default compose(
  connect(mapStateToProps, { getPlayerByHandle: actions.getPlayerByHandle }),
)(PlayerView);
