import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { ROUTE_PLAYDAYS } from 'const/Routes';
import { TXT_DATE, TXT_LOSS, TXT_RESULT, TXT_RESULTS, TXT_WIN } from 'const/Vars';
import React from 'react';

import { RouteComponentProps, withRouter } from 'react-router-dom';
import { IPlayerPlayday } from 'types/interface/IPlayer';

interface IProps extends RouteComponentProps {
  playdays: IPlayerPlayday[];
}

const useStyles = makeStyles((theme: Theme) => createStyles(
  {
    row: {
      '&:hover': {
        backgroundColor: theme.palette.grey['100'],
      },
      cursor: 'pointer',
    },
  }),
);

const PlayerPlaydayList: React.FC<IProps> = ({ playdays, history }) => {
  const classes = useStyles();

  if (!playdays.length) {
    return null;
  }

  const list = playdays.map(({ _id, name, playday, result }) => {
    return (
      <TableRow
        key={_id}
        onClick={() => history.push(`${ROUTE_PLAYDAYS}${playday}`)}
        className={classes.row}
      >
        <TableCell component="th" scope="row">
          <Typography color="textPrimary">{name}</Typography>
        </TableCell>
        <TableCell>
          {result > 0 && <Typography color="primary">{TXT_WIN}</Typography>}
          {result < 0 && <Typography color="error">{TXT_LOSS}</Typography>}
        </TableCell>
      </TableRow>
    );
  });

  return (
    <TableContainer component={Paper}>
      <Table aria-label={TXT_RESULTS}>
        <TableHead>
          <TableRow>
            <TableCell>{TXT_DATE}</TableCell>
            <TableCell>{TXT_RESULT}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {list}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default withRouter(React.memo(PlayerPlaydayList));
