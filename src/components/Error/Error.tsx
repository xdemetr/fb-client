import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import React from 'react';

// tslint:disable-next-line:function-name
function Alert(props: any) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

interface IProps {
  message?: string;
}

const Error: React.FC<IProps> = ({ message }) => {

  const [open, setOpen] = React.useState(true);

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

  if (!message) {
    return null;
  }

  return (
    <Snackbar open={open} autoHideDuration={3500} onClose={handleClose}>
      <Alert severity="error">
        {message}
      </Alert>
    </Snackbar>
  );
};

export default React.memo(Error);
