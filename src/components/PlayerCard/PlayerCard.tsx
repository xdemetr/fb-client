import { Avatar, Button, Card, CardActions, CardHeader, Link } from '@material-ui/core';
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';
import React from 'react';
import { NavLink } from 'react-router-dom';
import { ROUTE_EDIT_PLAYER, ROUTE_PLAYERS } from '../../const/Routes';
import { TXT_EDIT, TXT_LABEL_BOX, TXT_LABEL_DAMAGE } from '../../const/Vars';
import IPlayer from '../../types/interface/IPlayer';

interface IProps {
  auth: boolean | undefined;
  player: IPlayer;
  onClick?: () => void;
  showBox?: boolean;
  withLink?: boolean;
  selected?: string;
}

const PlayerCard: React.FC<IProps> = (props) => {
  const {
    player: { _id: id, box, damage, handle, image, name },
    auth,
    showBox,
    withLink,
    onClick,
    selected,
  } = props;

  const EditButton = () => {
    if (!auth) {
      return null;
    }

    return (
      <CardActions>
        <Button href={`${ROUTE_EDIT_PLAYER}${id}`} size="small">{TXT_EDIT}</Button>
      </CardActions>
    );
  };

  const DamageIcon = () => {
    if (!damage) return null;
    return <LocalHospitalIcon titleAccess={TXT_LABEL_DAMAGE}/>;
  };

  const SimpleCard = () => {
    return (
      <CardHeader
        action={<DamageIcon/>}
        avatar={<Avatar aria-label={name} src={image}/>}
        title={name}
        subheader={showBox && `${TXT_LABEL_BOX}: ${box}`}
      />
    );
  };

  if (withLink) {
    return (
      <Card variant="elevation">
        <Link to={`${ROUTE_PLAYERS}${handle}`} component={NavLink} color="primary">
          <SimpleCard/>
        </Link>
        <EditButton/>
      </Card>
    );
  }

  return (
    <Card variant="elevation" onClick={onClick} className={selected}>
      <SimpleCard/>
      <EditButton/>
    </Card>
  );
};

export default PlayerCard;
