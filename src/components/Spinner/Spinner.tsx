import { CircularProgress } from '@material-ui/core';
import React from 'react';
// import s from './Spinner.module.css';

const Spinner = () => {
  return (
    // <div className={s.spinner}>
    //   <div className={s.ldsCss}>
    //     <div className={s.ldsDualRing}>
    //       <div>&nbsp;</div>
    //     </div>
    //   </div>
    // </div>
    <CircularProgress/>
  );
};

export default Spinner;
