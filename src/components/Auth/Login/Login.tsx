import { Container } from '@material-ui/core';

import Error from 'components/Error';
import Spinner from 'components/Spinner';

import { TXT_PAGE_LOGIN } from 'const/Vars';
import React from 'react';

import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as actions from 'store/actions/auth';

import { getAuthReselect } from 'store/selectors/auth';
import { AppState } from 'store/store';
import PageTitle from '../../PageTitle';
import LoginForm from './LoginForm';

interface IProps {
  auth: {
    isAuth: boolean;
    error?: string;
    loading: boolean;
  };
  loginUser: (formData: { email: string, password: string }) => void;
}

const Login: React.FC<IProps> = ({ loginUser, auth: { isAuth, error, loading } }) => {

  const onSubmit = (formData: { email: string, password: string }) => {
    loginUser(formData);
  };

  if (loading) {
    return <Spinner/>;
  }

  if (isAuth) {
    return <Redirect to="/"/>;
  }

  return (
    <div className="login-page">
      <Container component="main" maxWidth="sm" disableGutters={true}>
        <PageTitle>
          {TXT_PAGE_LOGIN}
        </PageTitle>

        <Error message={error}/>
        <LoginForm onSubmit={onSubmit}/>
      </Container>
    </div>
  );
};

const mapStateToProps = (state: AppState) => ({
  auth: getAuthReselect(state),
});

export default connect(
  mapStateToProps,
  { loginUser: actions.loginUser },
)(React.memo(Login));
