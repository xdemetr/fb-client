import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import InputField from 'components/Form/InputField';

import {
  TXT_FIELD_EMAIL_INVALID,
  TXT_FIELD_REQUIRED,
  TXT_LABEL_EMAIL,
  TXT_LABEL_PASSWORD,
  TXT_LOGIN,
} from 'const/Vars';

import { useFormik } from 'formik';
import React from 'react';
import * as Yup from 'yup';

interface IProps {
  onSubmit: (formData: { email: string, password: string }) => void;
}

const useStyles = makeStyles((theme:any) => ({
  form: {
    marginTop: theme.spacing(2),
    width: '100%',
  },
  submit: {
    margin: theme.spacing(3, 0, 0),
  },
}));

const LoginForm: React.FC<IProps> = ({ onSubmit }) => {
  const loginFormOptions = useFormik({
    initialValues: {
      email: '',
      password: '',
    },
    onSubmit: (values) => {
      onSubmit(values);
    },
    validationSchema: Yup.object().shape({
      email: Yup.string().email(TXT_FIELD_EMAIL_INVALID).required(TXT_FIELD_REQUIRED),
      password: Yup.string().required(TXT_FIELD_REQUIRED),
    }),
  });

  const classes = useStyles();

  const { handleSubmit, ...props } = loginFormOptions;

  return (
    <form onSubmit={handleSubmit} className={classes.form}>
      <InputField label={TXT_LABEL_EMAIL} name="email" type="email" autoFocus={true} {...props} />

      <InputField label={TXT_LABEL_PASSWORD} name="password" type="password" {...props} />

      <Button
        type="submit"
        fullWidth={true}
        variant="contained"
        color="primary"
        className={classes.submit}
      >
        {TXT_LOGIN}
      </Button>
    </form>
  );
};

export default React.memo(LoginForm);
