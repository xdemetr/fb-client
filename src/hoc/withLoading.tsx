import React from 'react';

import Spinner from 'components/Spinner';

const withLoading = (loading: boolean) => (Component: any) => {

  if (loading) return <Spinner/>;

  return (props: any) => {
    return (
      <div>
        <Component {...props} />
      </div>
    );
  };
};

export default withLoading;
