import axios from 'axios';

import IPlayday from 'types/interface/IPlayday';
import IPlaydayList from 'types/interface/IPlaydayList';
import IPlayer from 'types/interface/IPlayer';

// axios.defaults.baseURL = 'http://localhost:5001/fb/api/';
axios.defaults.baseURL = 'https://xdemetr-fbserver.herokuapp.com/api/';

interface IAuthResolve {
  data: {
    success: boolean;
    token: string;
  };
}

interface IAuthLogin {
  email: string;
  password: string;
}

interface IGetPlayer {
  data: IPlayer[];
  pageCount: number;
}

interface IGetPlaydays {
  data: IPlaydayList[];
  pageCount: number;
}

export const authAPI = {
  // registration(username, email, password) {
  //   return axios.post(`users`, {username, email, password})
  // },
  login(userData: IAuthLogin): Promise<IAuthResolve> {
    return axios.post('users/login', userData);
  },
};

export const playerAPI = {
  getPlayerList(limit = 25, page = 1): Promise<{ data: IGetPlayer }> {
    return axios.get(`players?limit=${limit}&page=${page}`);
  },
  getPlayer(id: string): Promise<{ data: IPlayer }> {
    return axios.get(`players/${id}`);
  },
  getPlayerByHandle(handle: string): Promise<{ data: IPlayer }> {
    return axios.get(`players/handle/${handle}`);
  },
  addPlayer(player: IPlayer): Promise<{ data: IPlayer }> {
    return axios.post('players', player);
  },
  updatePlayer(id: string, data: IPlayer): Promise<{ data: IPlayer }> {
    return axios.put(`players/${id}`, data);
  },
  deletePlayer(id: string): Promise<any> {
    return axios.delete(`players/${id}`);
  },
};

export const playdayAPI = {
  addPlayday(teams: any): Promise<{ data: IPlayday }> {
    return axios.post('/playdays', teams);
  },
  getPlaydays(limit = 10, page = 1): Promise<{ data: IGetPlaydays }> {
    return axios.get(`/playdays?limit=${limit}&page=${page}`);
  },
  getPlayday(id: string): Promise<{ data: IPlayday }> {
    return axios.get(`/playdays/${id}`);
  },
  updatePlayday(id: string, data: any): Promise<{ data: IPlayday }> {
    return axios.put(`playdays/${id}`, data);
  },
};
